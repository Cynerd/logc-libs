/* SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright 2021-2022, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 * Copyright 2022, Karel Kočí <cynerd@email.cz>
 */
#include <event2/logc.h>
#include <stdio.h>
#include <errno.h>

LOG(event);


static void libevent_log(int severity, const char *msg) {
	// libevent message already contain error so we do not want to show it twice
	errno = 0;
	logc(log_event, severity + LL_DEBUG - EVENT_LOG_DEBUG, "libevent: %s", msg);
}

void logc_event_init() {
	event_set_log_callback(libevent_log);
}

void logc_event_cleanup() {
	event_set_log_callback(NULL);
}
