/* SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright 2021-2022, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 * Copyright 2022, Karel Kočí <cynerd@email.cz>
 */
#include <czmq_logc.h>
#include <stdio.h>
#include <errno.h>

#define SUITE "czmq"
#define DEFAULT_SETUP f_setup
#define DEFAULT_TEARDOWN f_teardown
#include "unittests.h"


void unittests_add_suite(Suite*);

FILE* orig_stderr;
char *stderr_data;
size_t stderr_len;

void f_setup() {
	errno = 0;
	orig_stderr = stderr;
	stderr = open_memstream(&stderr_data, &stderr_len);
	logc_czmq_init();
	log_set_level(log_czmq, LL_DEBUG);
}
void f_teardown() {
	ck_assert_int_eq(errno, 0);
	logc_czmq_cleanup();
	fclose(stderr);
	stderr = orig_stderr;
}

TEST_CASE(zmq) {}

TEST(zmq, zmq_error) {
	zsys_error("test message: %s", "foo");
	fflush(stderr);
	ck_assert_str_eq(stderr_data, "ERROR:czmq: ZMQ: test message: foo\n");
}
END_TEST

TEST(zmq, zmq_warning) {
	zsys_warning("test message: %s", "foo");
	fflush(stderr);
	ck_assert_str_eq(stderr_data, "WARNING:czmq: ZMQ: test message: foo\n");
}
END_TEST

TEST(zmq, zmq_notice) {
	zsys_notice("test message: %s", "foo");
	fflush(stderr);
	ck_assert_str_eq(stderr_data, "NOTICE:czmq: ZMQ: test message: foo\n");
}
END_TEST

TEST(zmq, zmq_info) {
	zsys_info("test message: %s", "foo");
	fflush(stderr);
	ck_assert_str_eq(stderr_data, "INFO:czmq: ZMQ: test message: foo\n");
}
END_TEST

TEST(zmq, zmq_debug) {
	zsys_debug("test message: %s", "foo");
	fflush(stderr);
	ck_assert_str_eq(stderr_data, "DEBUG:czmq: ZMQ: test message: foo\n");
}
END_TEST
