/* SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright 2021-2022, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 * Copyright 2022, Karel Kočí <cynerd@email.cz>
 */
#include <event2/logc.h>
#include <errno.h>

#define SUITE "event"
#define DEFAULT_SETUP NULL
#define DEFAULT_TEARDOWN NULL
#include "unittests.h"


TEST_CASE(event) {}

TEST(event, init_cleanup) {
	logc_event_init();
	ck_assert_int_eq(errno, 0);
	logc_event_cleanup();
	ck_assert_int_eq(errno, 0);
}
END_TEST

// TODO we should implement tests that trigger callback but how?
